			Extra HTTP Methods Details
			==========================

AcornHTTP is designed to be very flexible but it is limited to the classical
HTTP methods.

Since its original design a couple of other HTTP methods have been defined for
various purposes e.g. for WebDAV, CalDAV etc. which are more or less popular.
Many methods are in fact similar to the standard set (GET/PUT etc) and so
can be handled by changing AcornHTTP quite simply.

Every client which wants to use AcornHTTP with a non standard HTTP method
registers the method details via a protocol specific SWI.

When a client no longer makes usage of the registered HTTP non standard
method especally when qutting it has to deregister its method(s). This allows
other applications to reuse the method number for their own purposes.

SWI HTTP_RegisterMethod
=======================
On entry:
R0 - pointer to the name of non standard HTTP method (eg. REPORT)
R1 - method flags
     Bit
     0-1 type
         0 - Caller must setup everything
         1 - Behave like PUT/POST
         2 - Behave like GET
         3 - Unused
     2-31 reserved (0)

On exit:
R2 - opaque method number to use within other AcornHTTP SWIs

Any number in method range 1-127 may be returned as valid result except the
method number which are fixed assigned to HTTP standard methods (GET, HEAD,
POST, PUT, OPTIONS, TRACE, DELETE).

SWI HTTP_DeregisterMethod
=========================
On entry:
R0 - pointer to the name of non standard HTTP method (eg. REPORT)
R1 - method flags (as were set when registering)

No exit defined
